package ictgradschool.web.lab16.examples.exercise01;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class UserDetailsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String country = request.getParameter("country");
        String city = request.getParameter("city");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        // Header stuff
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>Web Lab 16 Examples - Sessions</title>");
        out.println("</head>");

        out.println("<body>");
        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes
        out.println("<ul>");
        out.println("<li>First Name: " + fname + "</li>");
        out.println("<li>Last Name: "+lname+"</li>");
        out.println("<li>Country: "+country+"</li>");
        out.println("<li>City: "+city+"</li>");
        out.println("</ul>");

        out.println("<body>");

        request.getSession().setAttribute("fname",fname);
        request.getSession().setAttribute("lname",lname);
        request.getSession().setAttribute("country",country);
        request.getSession().setAttribute("city",city);

//        Cookie cookieFname = new Cookie("cfname", URLEncoder.encode(fname, "UTF-8"));
//        Cookie cookieLname = new Cookie("clname", URLEncoder.encode(lname, "UTF-8"));
//        Cookie cookieCountry = new Cookie("ccountry", URLEncoder.encode(country, "UTF-8"));
//        Cookie cookieCity = new Cookie("ccity", URLEncoder.encode(city, "UTF-8"));
//        response.addCookie(cookieFname);
//        response.addCookie(cookieLname);
//        response.addCookie(cookieCountry);
//        response.addCookie(cookieCity);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
