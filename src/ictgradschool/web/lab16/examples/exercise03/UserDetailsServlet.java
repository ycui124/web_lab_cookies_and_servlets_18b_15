package ictgradschool.web.lab16.examples.exercise03;

import ictgradschool.web.lab16.examples.exercise05.POJO;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

public class UserDetailsServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String country = request.getParameter("country");
        String city = request.getParameter("city");

        POJO obj = new POJO();
        obj.setFlname(fname);
        obj.setLname(lname);
        obj.setCountry(country);
        obj.setCity(city);

        request.getSession().setAttribute("obj", obj);
//
//        response.setContentType("text/html");
//        PrintWriter out = response.getWriter();
//
//        // Header stuff
//        out.println("<!DOCTYPE html>");
//        out.println("<html lang=\"en\">");
//        out.println("<head>");
//        out.println("<meta charset=\"UTF-8\">");
//        out.println("<title>Web Lab 16 Examples - Sessions</title>");
//        out.println("</head>");
//
//        out.println("<body>");
//        out.println("<a href=\"index.html\">HOME</a><br>");
//
//        out.println("<h3>Data entered: </h3>");
//        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
//        //TODO - add the parameters from the form to session attributes
//
//        POJO pojo = (POJO) request.getSession().getAttribute("obj");
//
//        out.println("<ul>");
//        out.println("<li>First Name: " + pojo.getFlname() + "</li>");
//        out.println("<li>Last Name: " + pojo.getLname() + "</li>");
//        out.println("<li>Country: " + pojo.getCountry() + "</li>");
//        out.println("<li>City: " + pojo.getCity() + "</li>");
//        out.println("</ul>");
//
//        out.println("<body>");

//        request.getSession().setAttribute("fname", fname);
//        request.getSession().setAttribute("lname", lname);
//        request.getSession().setAttribute("country", country);
//        request.getSession().setAttribute("city", city);
        response.sendRedirect("/Sessions");


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
