package ictgradschool.web.lab16.examples.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class HitCounterServlet extends HttpServlet {
    private int count;

    //    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        if ("true".equals(req.getParameter("removeCookie"))) {

            //TODO - add code here to delete the 'hits' cookie
            System.out.println(req.getParameter("removeCookie"));
            Cookie remove = new Cookie("remove", URLEncoder.encode("0", "UTF-8"));
            resp.addCookie(remove);
            init();
            print(out, remove, req, resp);
        } else {

            //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie

            Cookie increase = new Cookie("increase", "" + count);
            resp.addCookie(increase);
            System.out.println(count);
            print(out, increase, req, resp);
            count++;
        }

        //TODO - use the response object's send redirect method to refresh the page


    }

    public void init() throws ServletException {
        count = 1;
    }

    private void print(PrintWriter out, Cookie c, HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {
        out.println("<p>Number of hits: <span id=\"numHits\">" + URLDecoder.decode(c.getValue(), "UTF-8") + "</span></p>");
        out.println("<div>");
        out.println("<form action=\"HitCounter\" method=\"GET\">");
        out.println(" <label for=\"removeCookie\"><input type=\"checkbox\" id=\"removeCookie\" "
                + "name=\"removeCookie\" value=\"true\"> Remove cookie</label ><br >");
        out.println("<input type = \"submit\" value = \"Hit me!\" >");
        out.println("</form >");
        out.println("</div >");


    }
}